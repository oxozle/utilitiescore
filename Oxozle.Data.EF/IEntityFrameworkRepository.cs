﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Oxozle.Data.EF
{
    public interface IEntityFrameworkRepository<T> : IRepository<T> where T : class
    {
        List<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
         Func<DbSet<T>, IQueryable<T>> includeAction = null);
    }
}
