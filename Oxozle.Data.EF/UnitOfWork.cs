﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Oxozle.Data.EF
{
    public abstract class UnitOfWork<T> : IDisposable where T : DbContext
    {
        protected readonly T _context;
        private bool disposed;
        private Dictionary<string, object> repositories;

        public UnitOfWork(T context)
        {
            _context = context;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            //  catch (DbEntityValidationException dbEx)
            //  {
            //      foreach (var validationErrors in dbEx.EntityValidationErrors)
            //      {
            //          foreach (var validationError in validationErrors.ValidationErrors)
            //          {
            //              Trace.TraceInformation("Property: {0} Error: {1}", validationError.
            //PropertyName, validationError.ErrorMessage);
            //          }
            //      }
            //  }
            catch (Exception exception)
            {

            }

        }

        public void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            disposed = true;
        }

        public IEntityFrameworkRepository<T> Repository<T>() where T : class
        {
            if (repositories == null)
            {
                repositories = new Dictionary<string, object>();
            }

            var type = typeof(T).Name;

            if (!repositories.ContainsKey(type))
            {
                var repositoryType = typeof(BaseRepository<,>);
                var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T), _context.GetType()), _context);
                repositories.Add(type, repositoryInstance);
            }
            return (IEntityFrameworkRepository<T>)repositories[type];
        }
    }
}
