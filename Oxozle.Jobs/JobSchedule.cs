﻿#region Usings

using System;

#endregion

namespace Oxozle.Jobs
{
    /// <summary>
    ///     Расписание службы
    /// </summary>
    public class JobSchedule
    {
        /// <summary>
        ///     Время запуска (например в понедельник, в 2 часа)
        /// </summary>
        public DateTime? StartTimeUtc { get; set; }

        /// <summary>
        ///     Период запуска (например каждые 5 минут, каждый 1 час 10 мин и т.д.)
        /// </summary>
        public TimeSpan PeriodicTime { get; set; }

        /// <summary>
        ///     Не запускать до этого времени в этот день
        /// </summary>
        public TimeSpan? NotBeforeTimeOfDay { get; set; }

        /// <summary>
        ///     Не запускать после этого времени в этот день
        /// </summary>
        public TimeSpan? NotAfterTimeOfDay { get; set; }

        /// <summary>
        ///     Работает по периоду, по этим дням
        /// </summary>
        public DayOfWeek[] Days { get; set; }

        /// <summary>
        ///     Тип периодизации
        /// </summary>
        public PeriodicType PeriodicType { get; set; } = PeriodicType.Timespan;

        /// <summary>
        ///     Работает если установлено значение PeriodicType не Timespan
        /// </summary>
        public int PeriodicValue { get; set; }
    }

    public enum PeriodicType
    {
        Timespan = 1,
        Days = 2,
        Month = 3
    }
}