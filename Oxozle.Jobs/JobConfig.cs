﻿#region Usings

using System.Collections.Generic;

#endregion

namespace Oxozle.Jobs
{
    /// <summary>
    ///     Настройки для служб
    /// </summary>
    public class JobConfig
    {
        public JobConfig()
        {
            IsEnabled = true;
        }

        /// <summary>
        ///     Расписание
        /// </summary>
        public JobSchedule Schedule { get; set; }

        /// <summary>
        ///     Включена ли служба
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        ///     True - задача должна выполниться после старта службы
        /// </summary>
        public bool ShouldRunAfterStart { get; set; }

        /// <summary>
        ///     Настройки конкретного джоба
        /// </summary>
        public Dictionary<string, object> Settings { get; set; }
    }
}