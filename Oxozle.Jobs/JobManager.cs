﻿#region Usings

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Oxozle.Utilities;

#endregion

namespace Oxozle.Jobs
{
    /// <summary>
    ///     Менеджер для работы со службами
    /// </summary>
    public class JobManager
    {
        private readonly ILogger _eventLogger;
        private readonly List<JobBase> _jobs = new List<JobBase>();
        private Timer _timer;

        public JobManager(ILogger logger)
        {
            _eventLogger = logger;
        }

        /// <summary>
        ///     Список всех служб
        /// </summary>
        public List<JobBase> Jobs => _jobs;

        /// <summary>
        ///     Запустить
        /// </summary>
        public void Start()
        {
            //Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-Ru");
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-Ru");


            var sb = new StringBuilder();

            foreach (var job in _jobs)
            {
                job.Initialize();
                job.OnUnhandledException += Job_OnUnhandledException;
                sb.Append($"[{job.Name}]");
                if (job.Schedule.StartTimeUtc != null)
                    sb.Append($" Время запуска: {job.Schedule.StartTimeUtc}");
                sb.Append($" Интервал: {job.Schedule.PeriodicTime.ToReadableString()}");
                sb.Append($" Следующий запуск: {job.NextStart}");
                sb.Append(Environment.NewLine);
            }
            _eventLogger?.Information(sb.ToString());
            _timer = new Timer(TimerElapsed, null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
        }

        private void Job_OnUnhandledException(object sender, Exception ex)
        {
            var job = (JobBase) sender;
            _eventLogger.Error($"{job.Name} error: " + ex);
        }


        private void TimerElapsed(object sender)
        {
            ProcessJobs();
        }

        private void ProcessJobs()
        {
            //обработка служб
            foreach (var job in Jobs)
            {
                if (job.IsEnabled && job.ShouldStart())
                    job.Start();
            }
        }
    }
}