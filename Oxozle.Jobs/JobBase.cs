﻿#region Usings

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Oxozle.Utilities;

#endregion

namespace Oxozle.Jobs
{
    /// <summary>
    ///     Базовый класс для служб
    /// </summary>
    public abstract class JobBase
    {
        private ILogger _evenLogger;
        private int _failsCount; //сколько раз возникла ошибка при запуске
        private bool _isEnabled = true;
        protected bool _isRunning;
        protected DateTime _nextStart;

        public JobBase()
        {
            var assemblyLocation = Assembly.GetEntryAssembly().Location;
            var path = Path.Combine(Path.GetDirectoryName(assemblyLocation), "job.json");

            LoadConfig(path);
        }

        /// <summary>
        ///     Логгер
        /// </summary>
        protected ILogger EventLogger => _evenLogger;

        /// <summary>
        ///     Запущена ли служба
        /// </summary>
        public bool IsRunning => _isRunning;

        /// <summary>
        ///     Расписание службы
        /// </summary>
        public JobSchedule Schedule { get; set; }

        /// <summary>
        ///     Настройки джоба
        /// </summary>
        public Dictionary<string, object> Settings { get; set; }

        /// <summary>
        ///     Название службы
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        ///     Активная ли служба
        /// </summary>
        public bool IsEnabled => _isEnabled;

        /// <summary>
        ///     Время следующего запуска
        /// </summary>
        public DateTime NextStart
        {
            get { return _nextStart; }
        }

        public event EventHandler<Exception> OnUnhandledException;

        public void Initialize()
        {
            if (Schedule == null)
                throw new Exception($"Расписание для службы {Name} не задано");

            CalcNextTime();
        }

        public void SetLogger(ILogger eventLogger)
        {
            _evenLogger = eventLogger;
        }

        public void Start()
        {
            _isRunning = true;

            _evenLogger.Information($"Запускается {Name}");

            var s = new Stopwatch();
            s.Start();

            try
            {
                //запускаем службу в отдельном потоке
                Task.Factory.StartNew(OnStart).ContinueWith(t =>
                {
                    s.Stop();
                    _isRunning = false;
                    _nextStart = _nextStart + Schedule.PeriodicTime;

                    if (t.Exception != null && OnUnhandledException != null)
                    {
                        foreach (var ex in t.Exception.InnerExceptions)
                        {
                            OnUnhandledException.Invoke(this, ex);
                        }

                        _evenLogger.Warning(
                            $"[{Name}] не удалось выполнить службу. Заняло {TimeSpan.FromMilliseconds(s.ElapsedMilliseconds).ToReadableString()}. Следующий запуск: {_nextStart}");
                    }
                    else
                        _evenLogger.Information(
                            $"[{Name}] служба завершена. Заняло {TimeSpan.FromMilliseconds(s.ElapsedMilliseconds).ToReadableString()}. Следующий запуск: {_nextStart}");
                });
            }
            catch (Exception ex)
            {
                _failsCount++;

                EventLogger.Error($"{Name} ошибка: " + ex);
                OnUnhandledException?.Invoke(this, ex);
            }

            if (_failsCount >= 5)
            {
                //при 5 неудачных запусках отключаем службу
                _evenLogger.Information($"Не удалось выполнить {Name} 5 или более раз, служба будет отключена");
                _isEnabled = false;
            }
        }

        /// <summary>
        ///     Нужно ли запустить службу в данный момент
        /// </summary>
        public bool ShouldStart()
        {
            //если запущена ничего не делаем
            if (_isRunning)
                return false;

            return DateTime.UtcNow >= _nextStart;
        }

        protected abstract void OnStart();


        protected void LoadConfig(string path)
        {
            if (File.Exists(path))
            {
                var config = JsonConvert.DeserializeObject<JobConfig>(File.ReadAllText(path));
                if (config != null)
                {
                    Schedule = config.Schedule;
                    Settings = config.Settings;
                    _isEnabled = config.IsEnabled;
                }
            }
        }

        protected void CalcNextTime()
        {
            var startTime = Schedule.StartTimeUtc ?? DateTime.UtcNow;

            var calculated = false;
            _nextStart = (startTime + Schedule.PeriodicTime);

            while (!calculated)
            {
                calculated = true;

                if (Schedule.NotBeforeTimeOfDay.HasValue && _nextStart.TimeOfDay < Schedule.NotBeforeTimeOfDay.Value)
                    calculated = false;

                if (Schedule.NotAfterTimeOfDay.HasValue && _nextStart.TimeOfDay > Schedule.NotAfterTimeOfDay.Value)
                    calculated = false;

                if (Schedule.Days != null && (Schedule.Days.Length > 0 && !Schedule.Days.Contains(_nextStart.DayOfWeek)))
                    calculated = false;

                if (_nextStart < DateTime.UtcNow)
                    calculated = false;

                if (!calculated)
                {
                    if (Schedule.PeriodicType == PeriodicType.Timespan)
                        _nextStart = _nextStart.Add(Schedule.PeriodicTime);
                    else if (Schedule.PeriodicType == PeriodicType.Days)
                        _nextStart = _nextStart.AddDays(Schedule.PeriodicValue);
                    else if (Schedule.PeriodicType == PeriodicType.Month)
                        _nextStart = _nextStart.AddMonths(Schedule.PeriodicValue);
                }
            }
        }
    }
}