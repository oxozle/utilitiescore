﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oxozle.Utilities.Web
{
    public static class Extensions
    {
        private const string SchemeDelimiter = "://";
        private const string PortDelimiter = ":";

        public static string HostWithScheme(this Uri uri)
        {
            if (uri.Port == 80 || uri.Port == 443)
            {
                return uri.Scheme + SchemeDelimiter + uri.Host;
            }
            return uri.Scheme + PortDelimiter + uri.Host + ":" + uri.Port;

        }
    }
}
