﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oxozle.Utilities;

namespace Oxozle.UtilitiesCore.Logger
{
    internal static class ConsoleColorHelper
    {
        public static void Color(this string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }


    public class ConsoleLogger : ILogger
    {
        public ConsoleLogger()
        {
        }

        //public ConsoleLogger(string applicationName)
        //{
        //}

        public void DebugMessage(string message)
        {
            message.Color(ConsoleColor.Gray);
        }

        public void Information(string message)
        {
            message.Color(ConsoleColor.White);
        }

        public void Warning(string message)
        {
            message.Color(ConsoleColor.Magenta);
        }

        public void Error(string message)
        {
            message.Color(ConsoleColor.Red);
        }

        public void Error(string message, Exception exception)
        {
            StringBuilder messageLogBuilder = new StringBuilder();
            messageLogBuilder.AppendFormat("Message: {0}", message);
            messageLogBuilder.AppendLine();
            messageLogBuilder.AppendLine();
            messageLogBuilder.AppendFormat("Error: {0}", exception.Message);
            messageLogBuilder.AppendLine();
            messageLogBuilder.AppendFormat("Stack Trace: {0}", exception.StackTrace);

            Error(messageLogBuilder.ToString());
        }
    }
}
