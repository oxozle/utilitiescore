﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oxozle.Utilities
{
    public interface ILogger
    {

        /// <summary>
        /// Сообщение которое необходимо только для отладки 
        /// и просмотра состояния.
        /// </summary>
        void DebugMessage(string message);

        /// <summary>
        /// Add information for the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Information(string message);


        /// <summary>
        /// Add warning for the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Warning(string message);


        /// <summary>
        /// Add error for the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Error(string message);


        /// <summary>
        /// Add errors for the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        void Error(string message, Exception exception);
    }
}
