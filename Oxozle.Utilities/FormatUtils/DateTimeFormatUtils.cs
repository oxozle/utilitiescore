﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Oxozle.Utilities
{
    public static class DateTimeFormatUtils
    {
        /// <summary>
        ///   Конвертирует время из юникса
        /// </summary>
        public static DateTime ConvertFromUnixTimestamp(this long timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }

        /// <summary>
        ///   Конвертирует время в юникс
        /// </summary>
        public static long ConvertToUnixTimestamp(this DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return (long)Math.Floor(diff.TotalSeconds);
        }

        /// <summary>
        ///   Возвращает True если дата - сегодна
        /// </summary>
        public static bool IsToday(this DateTime date)
        {
            DateTime now = DateTime.Now;

            if (now.Year == date.Year && now.Month == date.Month && now.Day == date.Day)
                return true;

            return false;
        }

        /// <summary>
        ///   Возвращает True если дата - сегодна
        /// </summary>
        public static bool IsYesterday(this DateTime date)
        {
            DateTime now = DateTime.Now;

            if (now.Year == date.Year && now.Month == date.Month && now.Day - 1 == date.Day)
                return true;

            return false;
        }

        //public static string GetShortDate(this DateTime? date)
        //{
        //    if (!date.HasValue)
        //        return string.Empty;

        //    return date.Value.ToShortDateString();
        //}

        /// <summary>
        ///   Время, когда произошло событие
        /// </summary>
        public static string GetTime(this DateTime date)
        {
            TimeSpan span = DateTime.Now - date;

            if (span.TotalSeconds <= 8)
                return "только что";

            if (span.TotalMinutes <= 1)
                return "менее минуты назад";

            if (span.TotalMinutes < 60)
                return (int)span.TotalMinutes + " " +
                       ((int)span.TotalMinutes).GetCountEnd("минуту", "минуты", "минут") + " назад";

            if (span.TotalHours < 24)
                return (int)span.TotalHours + " " + ((int)span.TotalHours).GetCountEnd("час", "часа", "часов") +
                       " назад";

            if (span.TotalDays < 10)
                return (int)span.TotalDays + " " + ((int)span.TotalDays).GetCountEnd("день", "дня", "дней") + " назад";

            if (span.TotalDays < 30)
                return (int)span.TotalDays + " " + ((int)span.TotalDays).GetCountEnd("день", "дня", "дней") + " назад";

            int month = (int)(span.TotalDays / 30);
            if (month < 12)
                return month + " " + month.GetCountEnd("месяц", "месяца", "месяцев") + " назад";

            return date.ToString(CultureInfo.CurrentUICulture.DateTimeFormat.FullDateTimePattern);
        }

        /// <summary>
        ///   Полное и точное время, когда произошло событие
        /// </summary>
        public static string GetFullTime(this DateTime date)
        {
            return date.ToString("dd.MM.yyyy") + " в " + date.ToString("H:mm");
        }

        ///// <summary>
        /////   Время, когда произошло событие
        ///// </summary>
        //public static string GetFutureTime(this DateTime date)
        //{
        //    DateTime now = DateTime.Now;


        //    if (now.DayOfYear == date.DayOfYear)
        //        return "сегодня";

        //    if (now.DayOfYear + 1 == date.DayOfYear)
        //        return "завтра";

        //    if (now.DayOfYear + 2 == date.DayOfYear)
        //        return "послезавтра";

        //    return date.ToShortDateString();
        //}

        public static string ToReadableAgeString(this TimeSpan span)
        {
            return string.Format("{0:0}", span.Days / 365.25);
        }

        public static string ToReadableString(this TimeSpan span)
        {
            string formatted = string.Format("{0}{1}{2}{3}",
                span.Duration().Days > 0 ? string.Format("{0:0} {1}, ", span.Days, span.Days.GetCountEnd("день", "два", "дней")) : string.Empty,
                span.Duration().Hours > 0 ? string.Format("{0:0} {1}, ", span.Hours, span.Hours.GetCountEnd("час", "часа", "часов")) : string.Empty,
                span.Duration().Minutes > 0 ? string.Format("{0:0} {1}, ", span.Minutes, span.Minutes.GetCountEnd("минута", "минуты", "минут")) : string.Empty,
                span.Duration().Seconds > 0 ? string.Format("{0:0} {1}", span.Seconds, span.Seconds.GetCountEnd("секунда", "секунды", "секунд")) : string.Empty);

            if (formatted.EndsWith(", ")) formatted = formatted.Substring(0, formatted.Length - 2);

            if (string.IsNullOrEmpty(formatted)) formatted = "0 секунд";

            return formatted;
        }

        public static string ToReadableStringShort(this TimeSpan span)
        {
            string formatted = string.Format("{0}{1}{2}{3}",
                span.Duration().Days > 0 ? string.Format("{0:0} д ", span.Days) : string.Empty,
                span.Duration().Hours > 0 ? string.Format("{0:0} ч ", span.Hours) : string.Empty,
                span.Duration().Minutes > 0 ? string.Format("{0:0} м, ", span.Minutes) : string.Empty,
                span.Duration().Seconds > 0 ? string.Format("{0:0} с", span.Seconds) : string.Empty);

            if (formatted.EndsWith(", ")) formatted = formatted.Substring(0, formatted.Length - 2);

            if (string.IsNullOrEmpty(formatted)) formatted = "0 с";

            return formatted;
        }

        public static DateTime GetFirstDayOfWeek(DateTime date)
        {
            var firstDayOfWeek = date.AddDays(-((date.DayOfWeek - DayOfWeek.Monday + 7) % 7));
            if (firstDayOfWeek.Year != date.Year)
                firstDayOfWeek = new DateTime(date.Year, 1, 1);
            return firstDayOfWeek;
        }

        public static DateTime GetLastDayOfWeek(DateTime date)
        {
            var lastDayOfWeek = date.AddDays((DayOfWeek.Sunday - date.DayOfWeek + 7) % 7);
            if (lastDayOfWeek.Year != date.Year)
                lastDayOfWeek = new DateTime(date.Year, 12, 31);
            return lastDayOfWeek;
        }
    }
}
