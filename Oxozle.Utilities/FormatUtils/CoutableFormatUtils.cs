﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oxozle.Utilities
{
    public static class CoutableFormatUtils
    {
        /// <summary>
        ///   Возвращает окончание исчисляемых
        /// </summary>
        /// <param name="count"> Кол-во </param>
        /// <param name="nominativeEnd"> Товар </param>
        /// <param name="genitiveEndOne"> Товара </param>
        /// <param name="genitiveEndMany"> Товаров </param>
        public static string GetCountEnd(this int count, string nominativeEnd, string genitiveEndOne,
            string genitiveEndMany)
        {
            if (count < 0)
                throw new ArgumentException("count");


            int lastDigit = count % 10;
            int lastTwoDigits = count % 100;
            if (lastDigit == 1 && lastTwoDigits != 11)
            {
                return nominativeEnd;
            }
            if (lastDigit == 2 && lastTwoDigits != 12 || lastDigit == 3 && lastTwoDigits != 13 ||
                lastDigit == 4 && lastTwoDigits != 14)
            {
                return genitiveEndOne;
            }
            return genitiveEndMany;
        }
    }
}
