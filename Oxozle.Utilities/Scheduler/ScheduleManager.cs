﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;


namespace Oxozle.Utilities
{
    public sealed class ScheduleManager
    {
        private readonly List<ScheduleJob> _jobs;
        private readonly object _lock = new object();

        private Timer _timer;

        private static ConsoleColor ScheduleManagerConsoleColor = ConsoleColor.Cyan;

        private ScheduleManager()
        {
            _jobs = new List<ScheduleJob>();
            _timer = new Timer(TimerElapsed, null, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
        }

        public static ScheduleManager Instance { get; } = new ScheduleManager();

        private void TimerElapsed(object sender)
        {
            lock (_lock)
            {
                var tasks = _jobs.Where(x => x.DateTrigger <= DateTime.Now).ToList();

                foreach (var scheduleJob in tasks)
                {
                    if (scheduleJob.ShouldLogSchedule)
                    {
                        "Start ".WriteColor(ScheduleManagerConsoleColor);
                        scheduleJob.ToString().WriteLineColor(ScheduleManagerConsoleColor);
                    }

                    _jobs.Remove(scheduleJob);
                    scheduleJob.Start();
                }
            }
        }


        public void Schedule(ScheduleJob job)
        {
            lock (_lock)
            {
                job.DateTrigger = DateTime.Now + job.ScheduleTo;
                _jobs.Add(job);

                if (job.ShouldLogSchedule)
                {
                    "Add ".WriteColor(ScheduleManagerConsoleColor);
                    job.ToString().WriteLineColor(ScheduleManagerConsoleColor);
                }
            }
        }


        public static string TaskId<T>(int id)
        {
            return $"{typeof(T)}_{id}";
        }

        public void Remove<T>(int id)
        {
            lock (_lock)
            {
                var taskId = TaskId<T>(id);
                _jobs.RemoveAll(x => x.TaskId == taskId);

                $"Remove {typeof(T)} with {taskId}".WriteLineColor(ScheduleManagerConsoleColor);
            }
        }

        //public void Remove(ScheduleJobType type, string objectId)
        //{
        //    lock (_lock)
        //    {
        //        _jobs.RemoveAll(x => x.JobType == type && x.TaskObjectId == objectId);

        //        $"Remove {type} with {objectId}".WriteLineColor(ScheduleManagerConsoleColor);
        //    }
        //}

        //public void Remove(ScheduleJobType type, int objectId)
        //{
        //    lock (_lock)
        //    {
        //        _jobs.RemoveAll(x => x.JobType == type && x.TaskObjectId == objectId.ToString());
        //        $"Remove {type} with {objectId}".WriteLineColor(ScheduleManagerConsoleColor);
        //    }
        //}

        public void ShowAllTasks()
        {
            lock (_lock)
            {
                foreach (ScheduleJob scheduleJob in _jobs)
                {
                    Console.WriteLine($"{scheduleJob} {scheduleJob.DateTrigger}");
                }
            }
        }
    }
}
