﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Oxozle.UtilitiesCore
{
    public static class ConvertUtils
    {
        private static readonly CultureInfo EnCulture = new CultureInfo("en-us");
        private static readonly CultureInfo RuCulture = new CultureInfo("ru-ru");


        public static decimal? GetDecimal(object value)
        {
            //todo check dbnull
            if (value == null)
                return null;


            string str = value.ToString();


            decimal result;

            //Try parsing in the current culture
            if (!decimal.TryParse(str, NumberStyles.Any, RuCulture, out result) &&
                //Then try in US english
                !decimal.TryParse(str, NumberStyles.Any, EnCulture, out result) &&
                //Then in neutral language
                !decimal.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                return null;
            }

            return result;
        }


        public static decimal GetDecimal(object value, decimal defaultValue)
        {
            //todo check dbnull
            if (value == null)
                return defaultValue;


            string str = value.ToString();


            decimal result;

            //Try parsing in the current culture
            if (!decimal.TryParse(str, NumberStyles.Any, RuCulture, out result) &&
                //Then try in US english
                !decimal.TryParse(str, NumberStyles.Any, EnCulture, out result) &&
                //Then in neutral language
                !decimal.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                result = defaultValue;
            }

            return result;
        }

    }
}
