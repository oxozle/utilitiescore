﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oxozle.Utilities
{
      public static class OXConsoleHelper
    {
        private static readonly ConsoleColor _color;

        static OXConsoleHelper()
        {
            _color = ConsoleColor.Gray;
        }

        public static void WriteColor(this string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ForegroundColor = _color;
        }

        public static void WriteLineColor(this string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = _color;
        }
    }
}
